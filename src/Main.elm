module Main exposing (..)

-- A management tool for HHRR that handles the vacations.
-- The workflows it handles:
--   - An employee requests some days. They go through a chain of responsibles until it is approved/rejected
--   - The vacations can be edited in a batch for a team for certain dates (christmas, summer...)
--
-- Features:
--   - The employee chooses some days in a calendar and clicks to request the vacations.
--   - A manager receives a notification (maybe email) to approve/refuse the vacations, with some optional comment
--   - The employee receives a notification once the vacations are finally approved or refused.
--   - An employee can save some vacations before requesting them.
--
-- TODO Create a mattermost bot

import Browser
import Browser.Navigation as Navigation
import Conditional.List as CList
import Date exposing (Date, Interval(..), Unit(..))
import Dict
import Element as E
import Element.Background as EBA
import Element.Border as EB
import Element.Font as F
import Element.Input as EI
import List.Extra as ListE
import Time exposing (Month(..))
import Url


type alias Flags =
    {}



-- The main function of the ELm program


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- Initializes the Model and sends and Cmd Msg's that should be sent upon
-- initialization of the poject


init : Flags -> Url.Url -> Navigation.Key -> ( Model, Cmd Msg )
init {} url key =
    ( { showDate = Just <| Date.fromCalendarDate 2020 Sep 20
      , message = ""
      , remoteData =
            Loaded
                { vacationsMap =
                    Dict.fromList
                        [ ( Date.fromCalendarDate 2020 Sep 23 |> Date.toRataDie
                          , { history =
                                [ { approvalChain =
                                        ApprovalChain
                                            { status = Pending
                                            , person = PersonID 1
                                            , next = Nothing
                                            }
                                  , comments = []
                                  , date = Date.fromCalendarDate 2020 Sep 12
                                  }
                                ]
                            , privateComments =
                                [ { id = PrivateCommentID 1, text = "Un comentario" }
                                , { id = PrivateCommentID 2, text = "Otro comentario" }
                                ]
                            , publicComments = []
                            }
                          )
                        , ( Date.fromCalendarDate 2020 Sep 24 |> Date.toRataDie
                          , { history = []
                            , privateComments = []
                            , publicComments = [ "A public comment", "Another public comment" ]
                            }
                          )
                        , ( Date.fromCalendarDate 2020 Sep 25 |> Date.toRataDie
                          , { history = []
                            , privateComments =
                                [ { id = PrivateCommentID 3, text = "Some private comment" }
                                , { id = PrivateCommentID 4, text = "Some other private comment" }
                                ]
                            , publicComments = [ "A public comment", "Another public comment" ]
                            }
                          )
                        ]
                , selectedDate = Nothing
                }
      }
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



---------------------------------------- MODEL/UPDATE ----------------------------------------
-- Data central to the application. This application has no data.


type alias Model =
    { showDate : Maybe Date.Date
    , message : String
    , remoteData : RemoteData
    }


type RemoteData
    = Loading
    | Loaded Data


type alias Data =
    { vacationsMap : VacationsMap
    , selectedDate : Maybe Date.Date
    }


type alias VacationsMap =
    Dict.Dict RataTime Vacations


type alias Vacations =
    { history : History
    , privateComments : List PrivateComment
    , publicComments : List String
    }


type alias PrivateComment =
    { id : PrivateCommentID
    , text : String
    }


type alias History =
    List VacationRequest


type alias VacationRequest =
    { approvalChain : ApprovalChain
    , comments : List Comment
    , date : Date
    }


type ApprovalChain
    = ApprovalChain { status : ApprovalStatus, person : PersonID, next : Maybe ApprovalChain }


type ApprovalStatus
    = Pending
    | Denied
    | Approved


type alias Comment =
    { text : String
    , person : PersonID
    }


type PersonID
    = PersonID Int


type PrivateCommentID
    = PrivateCommentID Int


type alias RataTime =
    Int


type Msg
    = UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | SelectDate Date.Date
    | WriteMessage String
    | AddPublicComment Date.Date String
    | AddPrivateComment Date.Date String
    | DeletePrivateComment { date : Date.Date, privateCommentID : PrivateCommentID }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked browser_request ->
            ( model, Cmd.none )

        UrlChanged url ->
            ( model, Cmd.none )

        SelectDate date ->
            case model.remoteData of
                Loading ->
                    ( model, Cmd.none )

                Loaded data ->
                    ( { model | remoteData = Loaded { data | selectedDate = Just date } }
                    , Cmd.none
                    )

        WriteMessage message ->
            ( { model | message = message }, Cmd.none )

        AddPublicComment date comment ->
            case model.remoteData of
                Loading ->
                    ( model, Cmd.none )

                Loaded data ->
                    ( { model
                        | message = ""
                        , remoteData =
                            Loaded
                                { data
                                    | vacationsMap =
                                        addPublicComment
                                            date
                                            comment
                                            data.vacationsMap
                                }
                      }
                    , Cmd.none
                    )

        AddPrivateComment date comment ->
            case model.remoteData of
                Loading ->
                    ( model, Cmd.none )

                Loaded data ->
                    ( { model
                        | message = ""
                        , remoteData =
                            Loaded
                                { data
                                    | vacationsMap =
                                        addPrivateComment
                                            date
                                            comment
                                            data.vacationsMap
                                }
                      }
                    , Cmd.none
                    )

        DeletePrivateComment keys ->
            case model.remoteData of
                Loading ->
                    ( model, Cmd.none )

                Loaded data ->
                    ( { model
                        | remoteData =
                            Loaded
                                { data
                                    | vacationsMap =
                                        deletePrivateComment keys data.vacationsMap
                                }
                      }
                    , Cmd.none
                    )


addPublicComment : Date.Date -> String -> VacationsMap -> VacationsMap
addPublicComment date comment =
    Dict.update (Date.toRataDie date)
        (\maybeVacations ->
            let
                vacations =
                    Maybe.withDefault emptyVacations maybeVacations
            in
            Just { vacations | publicComments = vacations.publicComments ++ [ comment ] }
        )


addPrivateComment : Date.Date -> String -> VacationsMap -> VacationsMap
addPrivateComment date comment =
    Dict.update (Date.toRataDie date)
        (\maybeVacations ->
            let
                vacations =
                    Maybe.withDefault emptyVacations maybeVacations

                max =
                    vacations.privateComments
                        |> List.map (\x -> x.id)
                        |> List.map (\(PrivateCommentID x) -> x)
                        |> List.maximum
                        |> Maybe.withDefault 1
            in
            Just
                { vacations
                    | privateComments =
                        vacations.privateComments
                            ++ [ { id = PrivateCommentID (max + 1), text = comment } ]
                }
        )


deletePrivateComment :
    { date : Date.Date, privateCommentID : PrivateCommentID }
    -> VacationsMap
    -> VacationsMap
deletePrivateComment { date, privateCommentID } =
    Dict.update (Date.toRataDie date)
        (\maybeVacations ->
            let
                vacations =
                    Maybe.withDefault emptyVacations maybeVacations
            in
            Just { vacations | privateComments = List.filter (\{ id } -> id /= privateCommentID) vacations.privateComments }
        )


emptyVacations : Vacations
emptyVacations =
    { history = [], privateComments = [], publicComments = [] }



---------------------------------------- VIEW ----------------------------------------


view : Model -> Browser.Document Msg
view model =
    { title = "Conhache"
    , body =
        [ E.layout
            [ E.width E.fill, E.height E.fill ]
          <|
            case model.remoteData of
                Loading ->
                    E.text "Loading..."

                Loaded ({ selectedDate, vacationsMap } as data) ->
                    case model.showDate of
                        Nothing ->
                            E.text "Loading..."

                        Just showDate ->
                            viewLoaded data showDate model.message
        ]
    }


viewLoaded : Data -> Date.Date -> String -> E.Element Msg
viewLoaded ({ selectedDate, vacationsMap } as data) showDate message =
    E.row
        [ E.centerX
        , E.centerY
        , E.spacing 4
        , E.width E.fill
        ]
        [ E.el
            [ E.centerX
            , E.centerY
            , F.bold
            , F.size 42
            , E.width E.fill
            ]
          <|
            E.column [ E.width E.fill, E.padding 10, E.spacing 10 ]
                [ showDate
                    |> Date.format "MMMM, yyyy"
                    |> E.text
                , viewMonth data showDate
                ]
        , E.el [ E.width E.fill, E.height E.fill, E.centerY ] <|
            case selectedDate of
                Nothing ->
                    E.text "Select a date!"

                Just theSelectedDate ->
                    E.column [ E.height E.fill ] <|
                        []
                            ++ (case
                                    Dict.get
                                        (Date.toRataDie theSelectedDate)
                                        vacationsMap
                                of
                                    Nothing ->
                                        [ E.text "No requested" ]

                                    Just vacations ->
                                        [ viewVacations theSelectedDate vacations ]
                               )
                            ++ [ E.column [ E.alignBottom, E.spacing 8 ] <|
                                    [ EI.text []
                                        { onChange = WriteMessage
                                        , text = message
                                        , placeholder = Nothing
                                        , label = EI.labelAbove [ F.italic ] <| E.text "a label"
                                        }
                                    , EI.button
                                        buttonStyle
                                        { label = E.text "Add public comment"
                                        , onPress = Just <| AddPublicComment theSelectedDate message
                                        }
                                    ]
                               , EI.button
                                    buttonStyle
                                    { label = E.text "Add private comment"
                                    , onPress = Just <| AddPrivateComment theSelectedDate message
                                    }
                               ]
        ]


buttonStyle =
    [ E.alignBottom, EBA.color <| E.rgb255 230 230 230, E.padding 8 ]


viewVacations : Date.Date -> Vacations -> E.Element Msg
viewVacations date { history, privateComments, publicComments } =
    E.column [ E.spacing 16, E.width E.fill ]
        ([]
            |> CList.attachIf
                (not <| List.isEmpty privateComments)
                (card [ E.width E.fill ]
                    { title = "Private Comments"
                    , content =
                        E.column [ E.spacing 4, E.width E.fill ] <|
                            List.map
                                (\{ text, id } ->
                                    E.row
                                        [ E.width E.fill ]
                                        [ E.text text
                                        , EI.button
                                            [ E.alignRight ]
                                            { label = E.text "x"
                                            , onPress =
                                                Just <|
                                                    DeletePrivateComment
                                                        { date = date
                                                        , privateCommentID = id
                                                        }
                                            }
                                        ]
                                )
                                privateComments
                    }
                )
            |> CList.attachIf
                (not <| List.isEmpty publicComments)
                (card []
                    { title = "Public Comments"
                    , content = E.column [ E.spacing 4 ] <| List.map E.text publicComments
                    }
                )
        )


card attrs { title, content } =
    E.column (attrs ++ [ E.spacing 8 ]) [ E.el [ F.bold ] <| E.text title, content ]


viewMonth : Data -> Date.Date -> E.Element Msg
viewMonth data date =
    let
        year =
            Date.year date

        month =
            Date.month date

        first =
            Date.floor Monday <| Date.fromCalendarDate year month 1

        last =
            Date.fromCalendarDate year month 40
    in
    E.table []
        { data = Date.range Monday 1 first last
        , columns =
            List.range 0 6
                |> List.map
                    (\inc ->
                        { header = E.text ""
                        , width = E.fill
                        , view = \monday -> viewDate data <| Date.add Days inc monday
                        }
                    )
        }


viewDate : Data -> Date.Date -> E.Element Msg
viewDate { selectedDate, vacationsMap } date =
    let
        isSelected =
            Maybe.map (\x -> x == date) selectedDate |> Maybe.withDefault False

        padding =
            14

        borderWidth =
            3
    in
    date
        |> Date.day
        |> String.fromInt
        |> E.text
        |> E.el [ E.centerX, E.centerY ]
        |> (\x ->
                EI.button
                    ([ EB.rounded 230
                     , EB.color <| E.rgb255 0 0 0
                     , E.centerX
                     , E.width <| E.minimum 70 E.shrink
                     ]
                        ++ (if isSelected then
                                [ EB.width <| borderWidth
                                , E.padding <| padding - borderWidth
                                ]

                            else
                                [ E.padding padding ]
                           )
                        ++ (case Dict.get (Date.toRataDie date) vacationsMap of
                                Nothing ->
                                    []

                                Just { history } ->
                                    case getStatus history of
                                        Nothing ->
                                            []

                                        Just status ->
                                            case status of
                                                Pending ->
                                                    [ EBA.color <| E.rgb255 220 220 220 ]

                                                Denied ->
                                                    [ EBA.color <| E.rgb255 250 10 10 ]

                                                Approved ->
                                                    [ EBA.color <| E.rgb255 250 10 250 ]
                           )
                    )
                    { label = x, onPress = Just <| SelectDate date }
           )
        |> E.el [ E.padding 5 ]


getStatus : History -> Maybe ApprovalStatus
getStatus history =
    ListE.last history |> Maybe.map (\x -> getApprovalStatus x.approvalChain)


getApprovalStatus : ApprovalChain -> ApprovalStatus
getApprovalStatus (ApprovalChain { status, next }) =
    case next of
        Nothing ->
            status

        Just nextChain ->
            getApprovalStatus nextChain
